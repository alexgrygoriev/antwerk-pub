'use strict';

module.exports = function(Post) {
  Post.observe('before save', function(ctx, next) {
    var instance = ctx.data || ctx.instance || ctx.currentInstance,


      query = {
        where: {
          and: [{
            language_id: instance.language_id
          }, {
            url: instance.url
          }]
        }
      }

    Post.find(query, (err, posts) => {
      if (err) {
        throw err
      }

      let found = false

      posts.forEach((post) => {
        //console.log(post.id + ', ' + instance.id + ', ' + post.language_id)
        if (post.id != instance.id) found = true
      })

      if (found) {
        let error = new Error()

        error.status = 500
        error.message = 'Такой Post URL уже существует!'
        error.code = 'SEO_URL_ALREADY_EXISTS'
        error.posts = posts
        error.inst = instance

        return next(error)
      } else {
        return next()
      }
    })
  });

};