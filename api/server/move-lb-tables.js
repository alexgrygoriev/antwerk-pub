var server = require('./server');
var ds = server.dataSources.MySQL;
var fs = require('fs');
var util = require('util');

// fs.writeFileSync('ds.js', util.inspect(ds), 'utf-8');
// console.log(server.dataSources);

var lbTables = ['User', 'AccessToken', 'ACL', 'RoleMapping', 'Role'];
ds.automigrate(lbTables, function(er) {
  if (er) throw er;
  console.log('Loopback tables [' - lbTables - '] created in ', ds.adapter.name);
  ds.disconnect();
});