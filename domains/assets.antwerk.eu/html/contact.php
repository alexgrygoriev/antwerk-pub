<?php

header('Access-Control-Allow-Origin: *');
// header('Access-Control-Allow-Credentials: true');
// header('Access-Control-Request-Method: GET, POST, OPTIONS');
// header('Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}');
header('Content-Type: application/json');

$json = [
    'success' => false,
    'errors'  => []
];

if (strlen($_POST['fullname']) < 7) $json['errors']['fullname'] = true;
if (strlen($_POST['sex']) < 1) $json['errors']['sex'] = true;
if (strlen($_POST['language']) < 2) $json['errors']['language'] = true;
if (strlen($_POST['service']) < 3) $json['errors']['service'] = true;
if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) $json['errors']['email'] = true;
if (strlen($_POST['telephone']) < 9) $json['errors']['telephone'] = true;
if (strlen($_POST['purpose']) < 3) $json['errors']['purpose'] = true;

if (!$json['errors']) {
    $toEmail = 'balloonia.ru@yandex.ru';
    $formEmail = $_POST['email'];

    $site = [
        'name'        => 'Antwerk',
        'url'         => 'http://antwerk.eu',
        'logo'        => '/image/main-logo.png',
        'title'       => 'Contacts from site: Antwerk'
    ];

    $subject = 'New contact feedback from ' . $site['name'];

    $html = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>' . $site['title'] . '</title>
    </head>
    <body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
        <div style="width: 680px;">
            <a href="' . $site['url'] . '" title="' . $site['name'] . '">
                <img src="' . $site['url'] . $site['logo'] . '" alt="' . $site['name'] . '" style="margin-bottom: 20px; border: none;" />
            </a>

            <p style="margin-top: 20px; margin-bottom: 20px;">User Details</p>

            <table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
                <thead>
                    <tr>
                        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">Client Details</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
                            <b>Фамилия, Имя:</b> ' . $_POST['fullname'] . '<br/>
                            <b>Пол:</b> ' . $_POST['sex'] . '<br/>
                            <b>Язык обращения:</b> ' . $_POST['language'] . '<br/>
                            <b>Требуемый вид услуг:</b> ' . $_POST['service'] . '<br/>
                            <b>Email:</b> ' . $_POST['email'] . '<br/>
                            <b>Контактный телефон:</b> ' . $_POST['telephone'] . '<br/>
                            <b>Цель обращения:</b> ' . $_POST['purpose'] . '<br/>
                        </td>
                    </tr>
                </tbody>
            </table>
            <p style="margin-top: 0px; margin-bottom: 20px;"></p>
        </div>
    </body>
    </html>
    ';

    require_once('./mail/mail.php');
    require_once('./mail.php');

    $mail = new Mail('mail');

    $mail->setTo($toEmail);
    $mail->setFrom($formEmail);
    $mail->setSender($site['name']);
    $mail->setSubject($subject);
    $mail->setHtml($html);
    $mail->send();

    $json['success'] = true;
}

echo json_encode($json);
//*/