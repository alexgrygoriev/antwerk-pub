<?php

header('Access-Control-Allow-Origin: *');
// header('Access-Control-Allow-Credentials: true');
// header('Access-Control-Request-Method: GET, POST, OPTIONS');
// header('Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}');
header('Content-Type: application/json');

function upload(&$attachments, $fieldName = 'file') {
	$json = array(
		'errors' => array()
	);

	$directory = '/var/www/antwerk.eu/html/data/files/';

	if (!is_dir($directory))
		$json['errors']['file'] = 'Warning: Directory does not exist!';

	if (!$json) {
		$files = array();

		if (!empty($_FILES[$fieldName]['name']) /*&& is_array($_FILES[$fieldName]['name'])*/ ) {
			// foreach (array_keys($_FILES[$fieldName]['name']) as $key) {
				$files[] = array(
					'name'	 => $_FILES[$fieldName]['name'],
					'type'	 => $_FILES[$fieldName]['type'],
					'tmp_name' => $_FILES[$fieldName]['tmp_name'],
					'error'	=> $_FILES[$fieldName]['error'],
					'size'	 => $_FILES[$fieldName]['size']
				);
			// }
		}

		foreach ($files as $file) {
			if (is_file($file['tmp_name'])) {
				$filename = basename(html_entity_decode($file['name'], ENT_QUOTES, 'UTF-8'));

				if ((strlen($filename) < 3) || (strlen($filename) > 255))
					$json['errors']['file'] = 'Warning: Filename must be between 3 and 255!';

				$extension = array(
					'jpg',
					'jpeg',
					'png',
					'pdf',
					'doc',
					'docx',
					'xls',
					'xlsx',
					'csv'
				);

				$fileExt = strtolower(substr(strrchr($filename, '.'), 1));

				if (!in_array($fileExt, $extension))
					$json['errors']['file'] = 'Warning: Incorrect file type!';

				$mime = array(
					'image/jpeg',
					'image/pjpeg',
					'image/png',
					'image/x-png',
					'application/vnd.ms-excel',
					'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
					'application/msword',
					'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
					'application/pdf',
					'text/csv'
				);

				if (!in_array($file['type'], $mime))
					if (!($file['type'] == 'application/octet-stream' && in_array($fileExt, ['doc', 'docx', 'xls', 'xlsx', 'csv'])))
						$json['errors']['file'] = 'Warning: Incorrect file type!';

				if ($file['error'] != UPLOAD_ERR_OK)
					$json['errors']['file'] = 'Warning: File could not be uploaded for an unknown reason!';
			} else {
				$json['errors']['file'] = 'Warning: File could not be uploaded for an unknown reason!';
			}

			if (!$json) {
				move_uploaded_file($file['tmp_name'], $directory . $filename);

				$attachments[] = $filename;
			}
		}
	}

	if (!$json) {
		$json['success'] = 'Success: Your file has been uploaded!';
	}

	return $json;
}

$files = [];

$json = upload($files);

if ($json['success']) $json['success'] = false;

if (strlen($_POST['fullname']) < 7) $json['errors']['fullname'] = true;
if (strlen($_POST['position']) < 1) $json['errors']['position'] = true;
if (strlen($_POST['country']) < 3) $json['errors']['country'] = true;
if (strlen($_POST['language']) < 2) $json['errors']['language'] = true;
if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) $json['errors']['email'] = true;
if (strlen($_POST['telephone']) < 9) $json['errors']['telephone'] = true;

if (!$json['errors']) {
	$toEmail = 'balloonia.ru@yandex.ru';
	$formEmail = $_POST['email'];

	$site = [
		'name'		=> 'Antwerk',
		'url'		 => 'http://antwerk.eu',
		'logo'		=> '/image/main-logo.png',
		'title'	   => 'Vacancy from site: Antwerk',
		'attachTitle' => 'Attached file',
	];

	$subject = 'New vacancy feedback from ' . $site['name'];

	$attachments = '';

	foreach ($files as $key => $file) {
		$attachments .= '
		<tr>
			<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><a target="_blank" href="' . $site['url'] . '/data/files/' . $file . '" title="' . $file . '">' . $file . '</a></td>
		</tr>';
	}

	$html = '
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
	<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>' . $site['title'] . '</title>
	</head>
	<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
		<div style="width: 680px;">
			<a href="' . $site['url'] . '" title="' . $site['name'] . '">
				<img src="' . $site['url'] . $site['logo'] . '" alt="' . $site['name'] . '" style="margin-bottom: 20px; border: none;" />
			</a>

			<p style="margin-top: 0px; margin-bottom: 20px;">' . $site['attachTitle'] . '</p>

			<table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
				<thead>
					<tr>
						<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">Attachment</td>
					</tr>
				</thead>
				<tbody>' . $attachments . '</tbody>
			</table>

			<p style="margin-top: 20px; margin-bottom: 20px;">User Details</p>

			<table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
				<thead>
					<tr>
						<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">Client Details</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
							<b>Name:</b> ' . $_POST['fullname'] . '<br/>
							' . (!empty(trim($_POST['position'])) ? '<b>Position:</b> ' . $_POST['position'] . '<br/>' : '') . '
							' . (!empty(trim($_POST['country'])) ? '<b>Country:</b> ' . $_POST['country'] . '<br/>' : '') . '
							' . (!empty(trim($_POST['language'])) ? '<b>Language:</b> ' . $_POST['language'] . '<br/>' : '') . '
							' . (!empty(trim($_POST['email'])) ? '<b>Email:</b> ' . $_POST['email'] . '<br/>' : '') . '
							' . (!empty(trim($_POST['telephone'])) ? '<b>Contact number:</b> ' . $_POST['telephone'] . '<br/>' : '') . '
						</td>
					</tr>
				</tbody>
			</table>
			<p style="margin-top: 0px; margin-bottom: 20px;"></p>
		</div>
	</body>
	</html>
	';


	require_once('./mail/mail.php');
	require_once('./mail.php');

	$mail = new Mail('mail');

	$mail->setTo($toEmail);
	$mail->setFrom($formEmail);
	$mail->setSender($site['name']);
	$mail->setSubject($subject);
	$mail->setHtml($html);
	$mail->send();

	$json['success'] = true;
}

echo json_encode($json);
//*/