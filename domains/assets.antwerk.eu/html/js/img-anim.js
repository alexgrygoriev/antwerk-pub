function csstransforms() {
    return 'WebkitTransform' in document.body.style || 
           'MozTransform' in document.body.style    || 
           'OTransform' in document.body.style      || 
           'transform' in document.body.style
}

function imageSequencer(path, count, options) {
  this.length = count;

  var self = this,

    images = [],
    numLoaded = 0,
    isComplete = false,

    defaultOptions = {
      indexSize: 4,
      initialStep: 64,
      onComplete: null,
      onProgress: null,
      stopAt: 1
    },

    pref = $.extend({}, defaultOptions, options),

    step = pref.initialStep,
    current = 0,
    hasRestepped = false;

  function callback(f, o) {
    if (!!f) f.apply(o);
  }

  this.stop = function() {
    step = pref.stopAt / 2;
  }

  this.reset = function() {
    current = numLoaded = 0;
    isComplete = hasRestepped = false;
    step = pref.initialStep;

    this.nearestIndex = -1;

    $.each(images, function(k, v) {
      !!v && v.unload();
    });
  }

  this.getAt = function(index) {
    return images[index].image;
  }

  this.nearestIndex = -1;

  this.getNearest = function(index) {
    index = Math.floor(index);

    var diff = 0,
      i, img;

    for (diff = 0; diff < images.length; diff++) {
      i = index + diff;

      if (i >= 0 && i < images.length) {
        img = images[i];

        if (img && img.isLoaded()) {
          this.nearestIndex = i;

          return img.image;
        }
      }

      i = index - diff;

      if (i >= 0 && i < images.length) {
        img = images[i];

        if (img && img.isLoaded()) {
          this.nearestIndex = i;

          return img.image;
        }
      }
    }

    return null;
  }

  // Loading
  this.getNumLoaded = function() {
    return numLoaded;
  }

  this.getLoadProgress = function() {
    return numLoaded * pref.stopAt / self.length;
  }

  this.isLoaded = function(index) {
    return index === undefined ?
      numLoaded == self.length :
      images[index].isLoaded();
  }

  this.loadPosition = function(position, complete) {
    self.loadIndex(Math.round(Math.min(1, Math.max(0, position)) * (self.length - 1)), complete);
  }

  this.loadIndex = function(index, complete) {
    if (index < 0 || index >= self.length) return false;
    if (index != Math.floor(index)) return false;

    if (images[index] == null)
      images[index] = new imageLoader(getSrcAt(index));

    images[index].load(function() {
      numLoaded++;

      if (!isComplete) {
        callback(pref.onProgress, this);
      }

      callback(complete, this);
    });
  }

  this.loadNext = function(complete) {
    if (step < pref.stopAt) return;

    function next() {
      loadNextImage();
      callback(complete, this);
    }

    function end() {
      finished();
      callback(complete, this);
    }

    current += step;

    if (current >= self.length) {
      if (hasRestepped) step /= 2;

      hasRestepped = true;
      current = step / 2;

      if (current >= pref.stopAt) {
        self.loadIndex(current, next);
      } else {
        finished();
      }
    } else {
      self.loadIndex(current, next);
    }
  }

  this.getImageLoader = function(index) {
    return images[index];
  }

  function loadNextImage() {
    setTimeout(function() {
      self.loadNext();
    }, $.browser.msie ? 50 : 5);
  }

  function finished() {
    isComplete = true;

    callback(pref.onComplete, this);
  }

  function getSrcAt(index) {
    return path.replace('{index}', (index + 1 + Math.pow(10, pref.indexSize)).toString(10).substr(1));
  }

  this.load = function() {
    self.loadIndex(0, loadNextImage);
  }
}

function imageLoader(src) {
  this.image = new Image();

  var loadStarted = false,
    self = this;

  this.getSrc = function() {
    return src;
  }

  this.load = function(complete) {
    loadStarted = true;

    self.image.src = src;

    if (self.image.complete)
      complete.apply(self.image);
    else
      $(self.image).on('load', complete);
  }

  this.unload = function() {
    loadStarted = false;
    self.image = new Image();
  }

  this.isLoaded = function() {
    return loadStarted && self.image.complete;
  }
}

$(document).ready(function(){
    var $windown = $(window),
        isChrome = navigator.userAgent.indexOf('Chrome') > 0,

        // dimensions
        windowHeight, windowWidth,
        fullHeight, scrollHeight,
        bgImgWidth = 768, bgImgHeight = 432;

    calculateDimensions();

    // states
    var currentPosition = getScrollTop() / scrollHeight,
        targetPosition = currentPosition,

        // collect timeline elements
        $videoImage = $('.fixed-video > img'),

        timeElements = [],
        $paralax = $('.paralax');

    // $('.has-paralax').each(function(){
    //     var height = 0, 
    //         padding = parseInt($(this).css('padding-left'));

    //     $(this).find('.paralax').each(function(){
    //         height += $(this).height() + parseInt($(this).css('margin-bottom'));

    //         var isLeft = $(this).hasClass('left');

    //         $(this).css({
    //             top: height + 'px',
    //             position: 'absolute'
    //         });

    //         $(this).css(isLeft ? 'left' : 'right', padding + 'px');

    //         timeElements.push({
    //             el: $(this),
    //             isLeft: isLeft,
    //             default: padding + 'px',
    //             top: $(this).offset().top,
    //             left: (isLeft ? $(this).offset().left : $(this).prev().offset().left) / 150
    //         });
    //     });

    //     $(this).height($(this).height() + height + 150);
    // });

    function setScrollTop(value) {
        $windown.scrollTop(value);
    }

    function getScrollTop() {
        return $windown.scrollTop() || (document.documentElement && document.documentElement.scrollTop);
    }

    function calculateDimensions() {
        windowWidth = $windown.width();
        windowHeight = $windown.height() + 100;
        fullHeight = $('.global-wrapper').height();
        scrollHeight = fullHeight - windowHeight;
    }

    function setTargetPosition(position, immediate) {
        targetPosition = position;

        if (immediate) currentPosition = targetPosition;
    }

    function handleResize() {
        calculateDimensions();
        renderBackgroundImage();
        renderTimeline(currentPosition);
        handleScroll();
    }

    function handleScroll() {
        setTargetPosition(getScrollTop() / scrollHeight);
    }

    // rendering

    function renderTimeline(position) {
        timeElements.forEach(function(paralax){
            var diff = paralax.top - getScrollTop();

            if (diff < 150) {
                paralax.el.css(paralax.isLeft ? 'left' : 'right', '-' + Math.abs(Math.ceil((diff - 150) * paralax.left)) + 'px');
            } else {
                paralax.el.css(paralax.isLeft ? 'left' : 'right', paralax.default);
            }
        });

        // video
        showImage(currentPosition);
    }

    function renderBackgroundImage(){
        // get image container size
        var scale = Math.max(windowHeight/bgImgHeight, windowWidth/bgImgWidth),
            width = scale * bgImgWidth, 
            height = scale * bgImgHeight,
            left = (windowWidth - width) / 2, top = (windowHeight - height) / 2;

        if ($.browser.safari && !isChrome) {
            var transform = 'translate3d(' + [-bgImgWidth / 2,-bgImgHeight / 2, 0].join('px,') + 'px) scale3d(' + scale + ',' + scale + ',1) translate3d(' + [windowWidth / 2 / scale, windowHeight / 2 / scale, 0].join('px,') + ')';

            $videoImage
                .width(bgImgWidth).height(bgImgHeight)
                .css('-webkit-transform',transform)
                .css({
                    'position': 'fixed',
                    top: 0,
                    left: 0});
        } else if (csstransforms()) {
            console.log('Using 2D transforms');

            var transform = 'translate(' + [-bgImgWidth / 2, -bgImgHeight / 2].join('px,')+'px) scale(' + scale + ') translate(' + [windowWidth / 2 / scale, windowHeight / 2 / scale].join('px,') + 'px)';

            $videoImage
                .width(bgImgWidth).height(bgImgHeight)
                .css({
                    '-webkit-transform': transform,
                    '-moz-transform': transform,
                    '-o-transform': transform,
                    '-ms-transform': transform,
                    'transform': transform
                })
                .css({
                    position: 'fixed',
                    top: 0,
                    left: 0
                });
        } else {
            $videoImage.width(width).height(height)
                .css({
                    position: 'fixed',
                    left: left + 'px',
                    top: top + 'px'
                });
        }
    }

    // main render loop
    window.requestAnimFrame = (function() {
      return  window.requestAnimationFrame       ||
              window.webkitRequestAnimationFrame ||
              window.mozRequestAnimationFrame    ||
              window.oRequestAnimationFrame      ||
              window.msRequestAnimationFrame     ||
              function(callback, element) {
                window.setTimeout(callback, 1000 / 60);
              };
    })();

    // video handling

    window.imageSeqLoader = new imageSequencer("//assets.gtagency.de/vid/img/" + window.frameFolder + "/small/{index}.jpg", window.frameCount, {
        indexSize: 4,
        onProgress: handleLoadProgress,
        onComplete: handleLoadComplete,
        stopAt: isSlowBrowser() ? 8 : 1
    });

    var loadCounterForIE = 0;

    imageSeqLoader.loadPosition(currentPosition, function(){
        if (++loadCounterForIE == 1) {
            showImage(currentPosition);

            imageSeqLoader.load();
            imageSeqLoader.load();
            imageSeqLoader.load();
            imageSeqLoader.load();
        }
    });

    function animloop() {
        requestAnimFrame(animloop);

        if (Math.floor(currentPosition*5000) != Math.floor(targetPosition * 5000)) {
            var deaccelerate = Math.max(Math.min(Math.abs(targetPosition - currentPosition) * 5000 , 10) , 2);

            currentPosition += (targetPosition - currentPosition) / deaccelerate;

            renderTimeline(currentPosition);
        }
    }

    animloop();

    var currentSrc,
        currentIndex,
        highresTimeout;

    function showImage(position) {
        var index = Math.round(currentPosition * (imageSeqLoader.length - 1)),
            img = imageSeqLoader.getNearest(index),
            $img = $(img),
            nearestIndex = imageSeqLoader.nearestIndex,
            src;

        if (nearestIndex < 0) nearestIndex = 0;

        if (!!img) {
            src = img.src;

            if (src != currentSrc) {
                $videoImage[0].src = src;
                currentSrc = src;
            }
        }

        clearTimeout(highresTimeout);

        highresTimeout = setTimeout(function(){
            if (!!src)
                loadHighres(src.split('/small/').join('/large/'));
        }, isSlowBrowser() ? 500 : 150);
    }

    var loadHighresCounter = 0;

    function loadHighres(src) {
        $videoImage[0].src = src;
    }

    $('body').append('<div id="loading-bar" style="position:fixed; z-index: 2000; bottom:0; left:0; background-color: #f7844a; background-color: rgba(247, 132, 74, 0.5); height: 1px;"></div>');

    function handleLoadProgress() {
        $('#loading-bar').css({
            width: imageSeqLoader.getLoadProgress() * 100 +'%',
            opacity: 1
        });
    }

    function handleLoadComplete() {
        $('#loading-bar').css({
            width: '100%',
            opacity: 0
        });
    }

    $windown.resize(handleResize);
    $windown.scroll(handleScroll);

    handleResize();

    function isSlowBrowser() {
        return $.browser.msie ? true : false;
    }
});
