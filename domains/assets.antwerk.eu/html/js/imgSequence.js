/**
 * Created by Hinderling Volkart AG.
 * User: sev
 * Date: 23.06.11
 * Time: 17:02
 * Copyright 2011, Hinderling Volkart AG. All rights reserved.
 */

function imgSequence(path, count, options) {
    var imgSeq = this,
        images = [],
        numLoaded = 0,
        isComplete = false,

        defaultOptions = {
            indexSize: 4 ,
            initialStep: 64 ,
            onComplete: null ,
            onProgress: null ,
            stopAt: 1
        },

        settings = {};

    $.extend(settings, defaultOptions, options);

    var step = settings.initialStep,
        current = 0,
        hasRestepped = false;

    this.length = count;

    function callback(f , o) {
        if (!!f) f.apply(o);
    }

    this.stop = function() {
        step = settings.stopAt / 2;
    };

    this.reset = function() {
        isComplete = false;
        numLoaded = 0;
        step = settings.initialStep;
        current = 0;
        hasRestepped = false;

        this.nearestIndex = -1;

        $.each(images , function(k,v) {
            !!v && v.unload();
        });
    };

    this.getAt = function( index ) {
        return images[index].image;
    };

    this.nearestIndex = -1;

    this.getNearest = function( index ) {
        index = Math.floor(index);

        var diff = 0,
            i, img;

        for (diff = 0; diff < images.length; diff++) {
            i = index + diff;

            if (i >= 0 && i < images.length) {
                img = images[i];

                if (img && img.isLoaded()) {
                    this.nearestIndex = i;

                    return img.image;
                }
            }

            i = index - diff;

            if (i >= 0 && i < images.length) {
                img = images[i];

                if (img && img.isLoaded()) {
                    this.nearestIndex = i;
                    return img.image;
                }
            }
        }

        return null;
    };

    // Loading

    this.getNumLoaded = function() {
        return numLoaded;
    };

    this.getLoadProgress = function() {
        return numLoaded * settings.stopAt / imgSeq.length;
    };

    this.isLoaded = function(index) {
        if ( index === undefined ) {
            return numLoaded == imgSeq.length;
        } else {
            return images[index].isLoaded();
        }
    };

    this.loadPosition = function(position , complete) {
        position = Math.min(1 , Math.max(0, position));

        var index = position * (imgSeq.length - 1);

        index = Math.round(index);
        imgSeq.loadIndex(index, complete);
    };

    this.loadIndex = function(index, complete) {
        if (index < 0 || index >= imgSeq.length)
            return false;

        if (index != Math.floor(index))
            return false;

        var img = images[index];

        if (img == null) {
            img = new ImageLoader(getSrcAt(index));
            images[index] = img;
        }

        img.load(function() {
            numLoaded++;

            if (!isComplete)
                callback(settings.onProgress, this);

            callback(complete, this);
        });
    };

    this.loadNext = function(complete) {
        if (step < settings.stopAt) return;
        
        function next() {
            loadNextImage();
            callback(complete, this);
        }

        function end() {
            finished();
            callback(complete, this);
        }

        current += step;

        if (current >= imgSeq.length) {
            if (hasRestepped) step /= 2;

            hasRestepped = true;
            current = step / 2;

            if (current >= settings.stopAt) {
                imgSeq.loadIndex(current,next);
            } else {
                finished();
            }
        } else {
            imgSeq.loadIndex(current,next);
        }
    };

    this.getImageLoader = function(index) {
        return images[index];
    };

    function loadNextImage() {
        setTimeout(function(){
            imgSeq.loadNext();
        }, $.browser.mozilla || $.browser.msie ? 50 : 5);
    }

    function finished() {
        isComplete = true;
        callback(settings.onComplete, this);
    }

    function getSrcAt(index) {
        return path.replace('{index}' , (index + 1 + Math.pow(10, settings.indexSize)).toString(10).substr(1));
    }

    this.load = function() {
        imgSeq.loadIndex(0, loadNextImage);
    }
}


function ImageLoader(src) {
    this.image = new Image();

    var img = this.image,
        loadStarted = false;

    this.getSrc = function() {
        return src;
    };
    
    this.load = function(complete) {
        loadStarted = true;
        img.src = src;

        if (img.complete) {
            complete.apply(img);
        } else {
            $(img).load(complete);
        }
    };

    this.unload = function() {
        loadStarted = false;
        img = this.image = new Image();
    };

    this.isLoaded = function() {
        return loadStarted && img.complete;
    }
}
