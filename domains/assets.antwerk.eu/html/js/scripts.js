$(function(){
	$('.features-box').height($('.features-box').width()*0.825);

	$(window).resize(function(){
		$('.features-box').height($('.features-box').width()*0.825);
	});

	wow = new WOW({
		boxClass: 'wow',
		animateClass: 'animated',
		offset: 0,
		mobile: false,
		live: true
	});

	wow.init();

})