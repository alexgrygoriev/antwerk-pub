<?php

header('Access-Control-Allow-Origin: *');

$json = [
    'success' => false,
    'error'   => true
];

if (isset($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    $res = mail('balloonia.ru@yandex.ru', 'У Вас новый подпишчик', 'Email: ' . $_POST['email']);

    $json['success'] = $res;
    $json['error'] = !$res;
}

echo json_encode($json);
