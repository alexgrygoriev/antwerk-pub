    //google.maps.event.addDomListener(window, 'load', init);
    function init() {
      var mapOptions = {
        zoom: 4,
        center: new google.maps.LatLng(50.6700, 15.9400), // New York
        styles: [{
          "featureType": "all",
          "elementType": "geometry.fill",
          "stylers": [{
            "weight": "2.00"
          }]
        }, {
          "featureType": "all",
          "elementType": "geometry.stroke",
          "stylers": [{
            "color": "#9c9c9c"
          }]
        }, {
          "featureType": "all",
          "elementType": "labels.text",
          "stylers": [{
            "visibility": "on"
          }]
        }, {
          "featureType": "landscape",
          "elementType": "all",
          "stylers": [{
            "color": "#f2f2f2"
          }]
        }, {
          "featureType": "landscape",
          "elementType": "geometry.fill",
          "stylers": [{
            "color": "#ffffff"
          }]
        }, {
          "featureType": "landscape.man_made",
          "elementType": "geometry.fill",
          "stylers": [{
            "color": "#ffffff"
          }]
        }, {
          "featureType": "poi",
          "elementType": "all",
          "stylers": [{
            "visibility": "off"
          }]
        }, {
          "featureType": "poi.business",
          "elementType": "labels.icon",
          "stylers": [{
            "visibility": "off"
          }]
        }, {
          "featureType": "road",
          "elementType": "all",
          "stylers": [{
            "saturation": -100
          }, {
            "lightness": 45
          }]
        }, {
          "featureType": "road",
          "elementType": "geometry.fill",
          "stylers": [{
            "color": "#eeeeee"
          }]
        }, {
          "featureType": "road",
          "elementType": "labels.text.fill",
          "stylers": [{
            "color": "#7b7b7b"
          }]
        }, {
          "featureType": "road",
          "elementType": "labels.text.stroke",
          "stylers": [{
            "color": "#ffffff"
          }]
        }, {
          "featureType": "road.highway",
          "elementType": "all",
          "stylers": [{
            "visibility": "simplified"
          }]
        }, {
          "featureType": "road.arterial",
          "elementType": "labels.icon",
          "stylers": [{
            "visibility": "off"
          }]
        }, {
          "featureType": "transit",
          "elementType": "all",
          "stylers": [{
            "visibility": "off"
          }]
        }, {
          "featureType": "water",
          "elementType": "all",
          "stylers": [{
            "color": "#46bcec"
          }, {
            "visibility": "on"
          }]
        }, {
          "featureType": "water",
          "elementType": "geometry.fill",
          "stylers": [{
            "color": "#c8d7d4"
          }]
        }, {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [{
            "color": "#070707"
          }]
        }, {
          "featureType": "water",
          "elementType": "labels.text.stroke",
          "stylers": [{
            "color": "#ffffff"
          }]
        }]
      };

      var mapElement = document.getElementById('map');
      var map = new google.maps.Map(mapElement, mapOptions);

      setMarkers(map)
    }

    var markers = [
      ['Krakow', 50.050992, 19.978741, 12],
      ['Brno', 49.202177, 16.656423, 12],
      ['Wiesbaden', 50.061997, 8.241606, 12],
      ['Tilburg', 51.566885, 5.083482, 12]
      // ['Marker4', 45.4628329, 9.1076922, 12]
    ];

    function setMarkers(map) {
      var image = {
        url: 'http://assets.gtagency.de/img/pin.png',
        size: new google.maps.Size(55, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 71)
      };
      var shape = {
        coords: [1, 1, 1, 20, 18, 20, 18, 1],
        type: 'poly'
      };
      for (var i = 0; i < markers.length; i++) {
        var beach = markers[i];
        var marker = new google.maps.Marker({
          position: {
            lat: beach[1],
            lng: beach[2]
          },
          map: map,
          icon: image,
          shape: shape,
          title: beach[0],
          zIndex: beach[3]
        });
      }
    }
