// Vue js Main script

const HomePage = { template: '<main></main>' }
const LogIn = { template: '<div>LogIn</div>' }

Vue.use(VueRouter)

const routes = [{
	path: '/',
	component: HomePage
}, {
	path: '/home',
	component: HomePage
}, {
	path: '/admin-login',
	component: LogIn
}]

const router = new VueRouter({
	routes // short for `routes: routes`
})

var app = new Vue({
	router,
	data: {
		message: 'Hello Vue!',
		title: 'Antwerk'
	}
}).$mount('#app')