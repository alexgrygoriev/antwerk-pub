import axios from 'axios';

export const HTTP = axios.create({
  baseURL: `//api.antwerk.eu:3100/api`,
  headers: {
    Authorization: 'Bearer {token}'
  }
})
