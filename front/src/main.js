import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import {
  HTTP
} from './http-common'

Vue.config.productionTip = false
Vue.config.devtools = false
Vue.config.silent = true

Vue.use(VueRouter)

import Page from './components/Page.vue'
import Admin from './components/Admin.vue'

import Home from './components/Home.vue'
import AboutUs from './components/AboutUs.vue'
import Vacancy from './components/Vacancy.vue'
import Complex from './components/Complex.vue'
import Industry from './components/Industry.vue'
import Contacts from './components/Contacts.vue'
import NotFound from './components/NotFound.vue'

import News from './components/News.vue'
import Post from './components/Post.vue'
import NewPost from './components/NewPost.vue'

import ServiceTransport from './components/ServiceTransport.vue'
import ServiceRail from './components/ServiceRail.vue'
import ServiceAir from './components/ServiceAir.vue'
import ServiceSea from './components/ServiceSea.vue'
import ServiceAdditional from './components/ServiceAdditional.vue'


const routes = [{
  path: '*',
  component: NotFound
}, {
  path: '/:locale(\\w\\w)?',
  component: Page,
  children: [{
    name: 'admin',
    path: 'admin',
    component: Admin
  }, {
    name: 'home',
    path: '',
    component: Home
  }, {
    name: 'about-us',
    path: 'about-us',
    component: AboutUs
  }, {
    name: 'vacancy',
    path: 'vacancy',
    component: Vacancy
  }, {
    name: 'complex',
    path: 'complex',
    component: Complex
  }, {
    name: 'industry',
    path: 'industry',
    component: Industry
  }, {
    name: 'contacts',
    path: 'contacts',
    component: Contacts
  }, {
    name: 'news',
    path: 'news',
    component: News
  }, {
    name: 'newpost',
    path: 'news/addpost',
    component: NewPost
  }, {
    name: 'post',
    path: 'news/:url',
    component: Post
  }, {
    name: 'transport',
    path: 'freight-transport',
    component: ServiceTransport
  }, {
    name: 'rail',
    path: 'rail-freight',
    component: ServiceRail
  }, {
    name: 'air',
    path: 'air-freight',
    component: ServiceAir
  }, {
    name: 'sea',
    path: 'sea-freight',
    component: ServiceSea
  }, {
    name: 'additional',
    path: 'additional-services',
    component: ServiceAdditional
  }]
}]

const router = new VueRouter({
  mode: 'history',
  routes, // short for `routes: routes`
  scrollBehavior(to, from, savedPosition) {
    if (to.hash) {
      return {
        selector: to.hash
      }
    } else if (savedPosition) {
      return savedPosition
    } else {
      return {
        x: 0,
        y: 0
      }
    }
  }
})

HTTP.get('languages').then(response => {
  let locale = 'en',
    languages = {},
    language_id = 0,
    host = location.hostname.toLowerCase().split('.'),
    pathName = location.pathname.toLowerCase().split('/').filter((el) => el.length > 0),
    hostLocale = host[host.length - 1]

  if (['pl', 'cz', 'de', 'nl'].indexOf(hostLocale) != -1)
    locale = hostLocale

  if (pathName.length && ['en', 'pl', 'cz', 'de', 'nl', 'ru'].indexOf(pathName[0]) != -1)
    locale = pathName[0]

  response.data.forEach((language) => {
    languages[language.code] = language
  })

  let rus = languages.ru

  delete languages.ru

  languages.ru = rus

  language_id = languages[locale].id

  new Vue({
    data() {
      return {
        isService: false,
        languages: languages,
        language_id: language_id,
        locale: locale,
        accessToken: '',
        hfInfo: {},
        errors: []
      }
    },

    created() {
      this.locale = this.$route.params.locale || this.locale
      this.language_id = this.languages[this.locale].id
      this.accessToken = localStorage.getItem('accessToken')

      document.documentElement.lang = this.locale

      this.getHFInfo()
      this.appendAnalytics(hostLocale)

      this.$root.$on('service_page', arg => {
        this.isService = arg
      })
    },

    watch: {
      $route(to, from) {
        this.locale = this.$route.params.locale || 'en'
        this.language_id = this.languages[this.locale].id

        this.getHFInfo()

        if (['sea', 'air', 'transport', 'rail', 'additional'].indexOf(to.name) == -1)
          this.isService = false
      }
    },

    methods: {
      getPage(page, language_id, cb) {
        let query = '?filter=' + encodeURIComponent(
          JSON.stringify({
            where: {
              and: [{
                name: page
              }, {
                language_id: language_id
              }]
            }
          })
        )

        HTTP.get('pages' + query).then(response => {
          if (cb) cb({
            info: JSON.parse(response.data[0].json),
            pageId: response.data[0].id,
            meta_title: response.data[0].meta_title,
            meta_description: response.data[0].meta_description
          })

          document.querySelector('title').text = response.data[0].meta_title.length
            ? response.data[0].meta_title
            : 'Antwerk'

          document.querySelector('meta[name="description"]').text = response.data[0].meta_description.length
            ? response.data[0].meta_description
            : ''

        }).catch(e => {
          this.errors.push(e)
        })
      },

      getHFInfo() {
        HTTP.get('settings?filter=' + encodeURIComponent(JSON.stringify({
          where: {
            and: [{
              key: 'header-footer'
            }, {
              language_id: this.language_id
            }]
          }
        }))).then(response => {
          if (response.data.length) {
            this.hfInfo = response.data[0].value.length ? JSON.parse(response.data[0].value) : {}
            this.hfInfo.id = response.data[0].id
            this.hfInfo.key = response.data[0].key
            this.hfInfo.language_id = response.data[0].language_id

            this.$root.$emit('map_data', this.hfInfo.contacts.mapdata)
          }
        }).catch(e => {
          this.errors.push(e)
        })
      },

      isAdmin() {
        return !!this.accessToken
      },

      dKey(key, pageName, pageId) {
        if (this.isAdmin()) {
          let attrs = []

          if (key != 'pageid') attrs = [{
            'data-key': key
          }]

          if (pageId)
            attrs = [{
              'data-id': pageId
            }, {
              'data-language': this.language_id
            }, {
              'data-page': pageName
            }]

          return attrs
        }
      },

      scriptLoader(src, cb) {
        var script = document.createElement('script')

        script.type = 'text/javascript'

        if (script.readyState) { //IE
          script.onreadystatechange = function() {
            if (script.readyState == 'loaded' || script.readyState == 'complete') {
              script.onreadystatechange = null

              if (typeof cb == 'function') cb()
            }
          }
        } else { //Others
          script.onload = function() {
            if (typeof cb == 'function') cb()
          }

          script.onerror = function() {
            console.log('Error loading ' + this.src)
          }
        }

        script.id = 'script-route-' + this.$route.name;

        script.src = src
        document.getElementsByTagName('head')[0].appendChild(script)
      },

      styleLoader(href, cb) {
        var link = document.createElement('link')

        link.rel = 'stylesheet'

        if (link.readyState) { //IE
          link.onreadystatechange = function() {
            if (link.readyState == 'loaded' || link.readyState == 'complete') {
              link.onreadystatechange = null

              if (typeof cb == 'function') cb()
            }
          }
        } else { //Others
          link.onload = function() {
            if (typeof cb == 'function') cb()
          }

          link.onerror = function() {
            console.log('Error loading ' + this.src)
          }
        }

        link.href = href
        document.getElementsByTagName('head')[0].appendChild(link)
      },

      appendAnalytics(host) {
        let ua = {
          'pl': 'UA-136135702-1',
          'cz': 'UA-136117846-1',
          'de': 'UA-136164939-1',
          'nl': 'UA-136172743-1',
          'eu': 'UA-136160339-1'
        }

        if (ua.hasOwnProperty(host)) {
          this.scriptLoader('https://www.googletagmanager.com/gtag/js?id=' + ua[host], () => {
            window.dataLayer = window.dataLayer || []

            function gtag(){
              dataLayer.push(arguments)
            }

            gtag('js', new Date())

            gtag('config', ua[host]);
          })
        }
      }
    },

    router,
    render: h => h(App),
    // mounted: () => document.dispatchEvent(new Event("x-app-rendered")),
    // mounted: () => document.dispatchEvent(new Event("x-app-rendered")),
  }).$mount('#app')

}).catch(e => {
  console.log(e)
})
